# Felodoro

> Pomodoro method application

# Prerequisites
> package name (version used in project)

npm (6.14.4)

node.js (v12.16.1), but it should come installed with npm




Default database connection config connects to the public cluster I have created for quick installation purposes.
Please switch to localhost mongodb if you can. Connection file path: server/database/connection.js





## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev
Open localhost:3000. Be ware this may result in conflicts if you are logged on felodoro.com already.
Opening this in private window is a safe bet.

Error occured?
-for example "Cannot find module '@babel/compat-data/corejs3-shipped-proposals"

Remove node_modules and run $ npm upgrade, $ npm install, $ npm run dev
Or try removing node modules and package-lock.json, then run $ npm install, $ npm run dev. 

```
database user:
	username: felodoro
	password: felodoro
