Aplikace je veřejně dostupná na adrese: https://gitlab.fel.cvut.cz/lipowada/pomodoro-method-application.git .

Pro instalaci je třeba mít nainstalovaný git a npm, naklonovat repositář a přesunout se do složky projektu s názvem felodoro. Poté stačí následovat README.md soubor.

příklad 
$git clone https://gitlab.fel.cvut.cz/lipowada/pomodoro-method-application
cd pomodoro-method-application/felodoro
($ npm upgrade) - pokud je verze node větší nebo rovna 13
$ npm install
$ npm run dev

Aplikace se spustí na url http://localhost:3000 .
Url doporučuji otevřít v anonymním okně. Při kombinaci s používáním nasazané aplikace na felodoro.herokuapp.com totiž dochází k problémům (localhost používá jinou databázi než produkce).  

Nyní máte plně funkční GraphQL server na adrese http://localhost:3000/graphql. Můžete zkusit, jak probíhá dotazování přes GraphQL. Nejrychlejší je v prohlížeči otevřít nástroj GraphiQL na adrese localhost:3000/graphiql (pozor! graphiql, ne graphql) a poté zkusit jeden z dotazů. Vpravo nahoře je tlačítko Docs, které vám pomůže.
Nejjednodušší dotaz je: 
query {
    userCount    
}

Přes prohlížeč nefungují subscriptions (bug v knihovnách)! Musíte použít nástroj vhodný pro GraphQL dotazování, jako například Altair (který vám doporučují tak či tak, nehledě na subscriptions).
Při dotazu na subscription budete muset zadat adresu socketu. Ta je u http serveru ws://localhost:3000/subscriptions .

Pokud při zadání příkazu "$ npm run dev" nastane problém, zkuste odstranit složku node_modules ($rm -rf node_modules) a: 
$ npm upgrade
$ npm install
$ npm run dev

Pokud ani toto nepomohlo, zkuste odstranit složku node_modules a také soubor package-lock.json a proveďte:
$ npm install
$ npm run dev



